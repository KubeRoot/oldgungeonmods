﻿using ETGGUI;
using SGUI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using static PlayerStats;

namespace StatDisplay
{
    class StatTrackLabel : SModifier
    {
        private string _name;
        private StatType _stat;
        public StatTrackLabel(StatType stat, string name)
        {
            _stat = stat;
            _name = name;
        }

        public override void Update()
        {
            SLabel obj = (SLabel)Elem;
            if(GameManager.Instance.PrimaryPlayer!=null)
            {
                obj.Text = _name + ": " + GameManager.Instance.PrimaryPlayer.stats.GetStatValue(_stat);
            }
            else
            {
                obj.Text = _name + ": No primary player found";
            }
            Elem.Visible = StatDisplay.displayStats;
        }
        /*
        public override void UpdateStyle(SElement elem)
        {
            elem.Size = new Vector2(512f, elem.Backend.LineHeight);
        }
        //*/
    }

    public class StatDisplay : ETGModule
    {
        //SLabel curselabel;
        //SLabel coolnesslabel;

        public static IList<SLabel> labels = new List<SLabel>();
        public static Font statLabelFont;
        public static bool displayStats = true;

        private void OnStatLabelUpdateStyle(SElement elem)
        {
            SLabel previous = labels[labels.IndexOf((SLabel)elem) - 1];
            elem.Position.y = previous.Position.y+previous.Size.y-18;

            if (statLabelFont == null)
                statLabelFont = FontConverter.GetFontFromdfFont((dfFont)GameUIRoot.Instance.Manager.DefaultFont, 1);

            elem.Font = statLabelFont;
        }

        private void ToggleDisplay(string[] args)
        {
            displayStats = !displayStats;
            ETGModConsole.Log("StatDisplay " + (displayStats ? "enabled" : "disabled"));
        }

        public override void Start()
        {
            IList<StatType> stats = (IList<StatType>)Enum.GetValues(typeof(StatType));
            labels.Insert(0, new SLabel(stats[0].ToString())
            {
                Position = new Vector2(10, 30),
                With =
                {
                    new StatTrackLabel(stats[0], stats[0].ToString())
                },
                OnUpdateStyle = delegate(SElement elem)
                {
                    if(statLabelFont==null)
                        statLabelFont = FontConverter.GetFontFromdfFont((dfFont)GameUIRoot.Instance.Manager.DefaultFont, 1);

                    elem.Font = statLabelFont;

                    float posy1 = elem.Position.y;
                    int max = labels.Count-1;
                    posy1 = (elem.Root.Size.y + posy1 - labels[max].Position.y - labels[max].Size.y) / 2f;
                    elem.Position.y = posy1;
                }
            });
            SGUIRoot.Main.Children.Add(labels[0]);

            int i = 1;
            StatType first = stats[0];

            foreach (StatType stat in stats)
            {
                if (stat==first) continue;
                labels.Insert(i, new SLabel(stat.ToString())
                {
                    Position = new Vector2(10, 30),
                    With =
                    {
                        new StatTrackLabel(stat, stat.ToString())
                    },
                    OnUpdateStyle = OnStatLabelUpdateStyle,
                });
                SGUIRoot.Main.Children.Add(labels[i]);
                i++;
            }

            SDModifier tmp = new SDModifier();
            tmp.OnUpdate += delegate (SElement elem)
            {
                SLabel label = elem as SLabel;
                elem.Visible = StatDisplay.displayStats;
                if (GameManager.Instance.PrimaryPlayer == null)
                    label.Text = "Magnificience: No primary player found";
                else
                    label.Text = "Magnificience: " + GameManager.Instance.PrimaryPlayer.stats.Magnificence;
            };

            labels.Insert(i, new SLabel("Magnificence")
            {
                Position = new Vector2(10, 30),
                OnUpdateStyle = OnStatLabelUpdateStyle,

                With =
                {
                    tmp
                }
            });
            SGUIRoot.Main.Children.Add(labels[i]);

            //i++;
            
            //i--;

            float posy = labels[0].Position.y;
            posy = (labels[0].Root.Size.y + posy - labels[i].Position.y - labels[i].Size.y) / 2f;
            labels[0].Position.y = posy;

            ETGModConsole.Commands.AddUnit("togglestats", ToggleDisplay);
        }

        public override void Init()
        {
        }

        public override void Exit()
        {
        }
    }
}
