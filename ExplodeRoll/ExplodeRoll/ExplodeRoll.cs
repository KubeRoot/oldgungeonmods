﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ExplodeRoll
{
    public class ExplodeRoll : ETGModule
    {
        private ExplosionData _data;

        /*
        public static bool enabled
        {
            get;
            private set;
        } = false;*/

        private static bool enabled;

        private void OnDodgeRoll(PlayerController ply)
        {
            if(ExplodeRoll.enabled)
                Exploder.Explode(ply.transform.position, _data, new Vector2(0, 0));
        }

        private void OnNewPlayer(Component component)
        {
            (component as PlayerController).OnPreDodgeRoll += OnDodgeRoll;
        }

        public override void Start()
        {
            ExplodeRoll.enabled = true;
        }

        public override void Init()
        {
            ETGMod.Objects.AddHook<PlayerController>(OnNewPlayer);

            Debug.Log(1);
            _data = new ExplosionData();
            _data.CopyFrom(GameManager.Instance.Dungeon.sharedSettingsPrefab.DefaultExplosionData);
            Debug.Log(2);

            _data.useDefaultExplosion = false;
            _data.force = 0;
            _data.breakSecretWalls = false;
            _data.damageToPlayer = 0f;
            _data.explosionDelay = 0.2f;
            Debug.Log(3);
        }

        public override void Exit()
        {
            enabled = false;
        }
    }
}
