﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace AllJammed
{
    public class AllJammed : ETGModule
    {
        private static bool enabled;

        private void makeJammed(AIActor enemy)
        {
            if(enabled && enemy.IsNormalEnemy)
                enemy.ForceBlackPhantom = true;
        }

        public override void Start()
        {
            enabled = true;
        }

        public override void Exit()
        {
            enabled = false;
        }

        public override void Init()
        {
            ETGMod.AIActor.OnBlackPhantomnessCheck += makeJammed;
        }
    }
}
