﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Spun
{
    class SpinBehavior : MonoBehaviour
    {
        Camera cam;
        static PlayerController ply;
        static MethodInfo handleInput;
        static float ang;
        static Vector2 m_dir;

        void OnDodgeRollStarted(PlayerController player, Vector2 dir)
        {
            m_dir = dir.Rotate(ang);
        }

        public void Update()
        {
            if(!Spun.isEnabled)
                return;

            ang = (float)Math.Sin(BraveTime.ScaledTimeSinceStartup) * 45;

            cam.transform.rotation = Quaternion.Euler(0, 0, ang);

            if (ply != GameManager.Instance.PrimaryPlayer)
            {
                ply = GameManager.Instance.PrimaryPlayer;
                ply.OnRollStarted += OnDodgeRollStarted;
            }
            
            if (ply != null)
            {
                if (ply.CurrentInputState == PlayerInputState.NoInput)
                {
                    ply.usingForcedInput = false;
                }
                else if(!ply.IsDodgeRolling)
                {
                    ply.usingForcedInput = true;
                    PlayerInputState oldState = ply.CurrentInputState;
                    ply.CurrentInputState = PlayerInputState.OnlyMovement;
                    ply.forcedInput = ((Vector2)handleInput.Invoke(ply, null)).Rotate(ang);
                    ply.CurrentInputState = oldState;
                }
                else
                {
                    ply.forcedInput = m_dir;
                }
            }
            Pixelator.Instance.DoOcclusionLayer = false;
        }

        public void Awake()
        {
            cam = GetComponent<Camera>();
            handleInput = (typeof(PlayerController)).GetMethod("HandlePlayerInput", BindingFlags.NonPublic | BindingFlags.Instance);
        }
    }

    public class Spun : ETGModule
    {
        private static bool enabled;
        public static bool isEnabled
        {
            get
            {
                return enabled;
            }
        }

        private void OnCameraController(Component component)
        {
            CameraController cam = component as CameraController;
            if(cam.Camera.GetComponent<SpinBehavior>() == null)
                cam.Camera.gameObject.AddComponent<SpinBehavior>();
        }

        public override void Init()
        {
            ETGMod.Objects.AddHook<CameraController>(OnCameraController);
        }

        public override void Start()
        {
            enabled = true;
        }

        public override void Exit()
        {
            enabled = false;
        }
    }
}
