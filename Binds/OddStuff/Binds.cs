﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using static ETGModConsole;

namespace Binds
{
    public class Bind
    {
        public bool isKeyCode;
        public string Key;
        public KeyCode KeyCode;
        public ConsoleCommandUnit Command;
        public string[] args;
        public bool isValid;

        public Bind(string key, string[] fullcmd)
        {
            ConsoleCommandGroup.UnitSearchResult results = Commands.SearchUnit(fullcmd);
            Command = results.unit;
            isKeyCode = false;
            Key = key;
            args = fullcmd.Skip(results.index+1).ToArray();
            isValid = results.index != -1;
        }
        public Bind(KeyCode key, string[] fullcmd)
        {
            ConsoleCommandGroup.UnitSearchResult results = Commands.SearchUnit(fullcmd);
            Command = results.unit;
            isKeyCode = true;
            KeyCode = key;
            args = fullcmd.Skip(results.index+1).ToArray();
            isValid = results.index != -1;
        }

        public bool IsPressed()
        {
            return isKeyCode ? Input.GetKeyDown(KeyCode) : Input.GetKeyDown(Key);
        }

        public void Run()
        {
            Command.RunCommand(args);
        }
    }
    public class BindListener : MonoBehaviour
    {
        public static List<Bind> Binds = new List<Bind>();
        
        public static void AddBind(Bind toAdd)
        {
            bool isKeyCode = toAdd.isKeyCode;
            foreach(Bind bind in Binds)
            {
                if(isKeyCode?(bind.KeyCode==toAdd.KeyCode):(bind.Key==toAdd.Key))
                {
                    Log("Key " + (isKeyCode ? toAdd.KeyCode.ToString() : toAdd.Key) + " is already bound");
                    return;
                }
            }
            Binds.Add(toAdd);
            Log("Key " + (isKeyCode ? toAdd.KeyCode.ToString() : toAdd.Key) + " was bound");
        }
        public static void RemoveBind(Bind toRemove)
        {
            Binds.Remove(toRemove);
        }
        public static void RemoveBind(string key)
        {
            foreach (Bind bind in Binds)
            {
                if (bind.Key == key)
                {
                    Binds.Remove(bind);
                    Log("Bind under key " + key + " was unbound");
                    return;
                }
            }
            Log("No bind under key " + key + " was found");
        }
        public static void RemoveBind(KeyCode key)
        {
            foreach (Bind bind in Binds)
            {
                if (bind.KeyCode == key)
                {
                    Binds.Remove(bind);
                    Log("Bind under keycode " + key + " was unbound");
                    return;
                }
            }
            Log("No bind under keycode " + key + " was found");
        }

        public void Update()
        {
            foreach(Bind bind in Binds)
            {
                if (bind.IsPressed()) bind.Run();
            }
        }
    }
    public class Binds : ETGModule
    {
        public static void BindKey(string[] args)
        {
            if (!ArgCount(args, 2)) return;
            Bind bind = new Bind(args[0], args.Skip(1).ToArray());
            if (bind.isValid)
            {
                BindListener.AddBind(bind);
            }
            else
            {
                Log("Invalid command");
            }
        }

        public static void BindKeyCode(string[] args)
        {
            if (!ArgCount(args, 2)) return;
            try
            {
                KeyCode key = (KeyCode)Enum.Parse(typeof(KeyCode), args[0], true);
                Bind bind = new Bind(key, args.Skip(1).ToArray());
                if (bind.isValid)
                {
                    BindListener.AddBind(bind);
                }
                else
                {
                    Log("Invalid command");
                }
            }
            catch(ArgumentException)
            {
                Log("Invalid keycode");
            }
        }

        public static void UnBindKey(string[] args)
        {
            if (!ArgCount(args, 1, 1)) return;
            BindListener.RemoveBind(args[0]);
        }

        public static void UnBindKeyCode(string[] args)
        {
            if (!ArgCount(args, 1, 1)) return;
            try
            {
                KeyCode key = (KeyCode)Enum.Parse(typeof(KeyCode), args[0], true);
                BindListener.RemoveBind(key);
            }
            catch (ArgumentException)
            {
                Log("Invalid keycode");
            }
        }

        public override void Exit()
        {
        }

        public override void Init()
        {
        }

        public override void Start()
        {
            Commands.AddGroup("bind");
            Commands.GetGroup("bind")
                .AddUnit("key", BindKey)
                .AddUnit("keycode", BindKeyCode);

            Commands.AddGroup("unbind");
            Commands.GetGroup("unbind")
                .AddUnit("key", UnBindKey)
                .AddUnit("keycode", UnBindKeyCode);

            ETGModMainBehaviour.Instance.gameObject.AddComponent<BindListener>();
        }
    }
}
