﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace InstaKill
{
    class InstaKillOnDamage : MonoBehaviour
    {
        private HealthHaver _healthHaver;
        private bool _wasHit = false;

        private static void OnHealthChanged(float resultValue, float maxValue) { }

        private void OnDamageInstagib(float resultValue, float maxValue, CoreDamageTypes damageTypes, DamageCategory damageCategory, Vector2 damageDirection)
        {
            if (InstaKill.isEnabled && !_wasHit)
            {
                _wasHit = true;

                _healthHaver.Armor = 0f;
                _healthHaver.ForceSetCurrentHealth(0);
                _healthHaver.Die(damageDirection);
            }
        }

        public void Start()
        {
            _healthHaver = GetComponent<HealthHaver>();

            _healthHaver.OnDamaged += OnDamageInstagib;
            _healthHaver.OnHealthChanged += OnHealthChanged;
        }
    }

    public class InstaKill : ETGModule
    {
        //private PlayerController ply;
        //private PlayerController ply2;

        private static bool enabled;

        public static bool isEnabled
        {
            get
            {
                return enabled;
            }
        }

        private void AddOnDamageInstagib(GameActor actor)
        {
            //new InstaKillOnDamage(actor.healthHaver);
            if(actor.healthHaver.GetComponent<InstaKillOnDamage>() == null)
                actor.healthHaver.gameObject.AddComponent<InstaKillOnDamage>();
        }

        private void OnNewPlayer(Component component)
        {
            AddOnDamageInstagib(component as PlayerController);
        }

        private void OnNewAIActor(Component component)
        {
            AddOnDamageInstagib(component as AIActor);
        }

        public override void Init()
        {
            ETGMod.Objects.AddHook<PlayerController>(OnNewPlayer);
            ETGMod.Objects.AddHook<AIActor>(OnNewAIActor);
        }

        public override void Start()
        {
            enabled = true;
        }
        
        public override void Exit()
        {
            enabled = false;
        }
    }
}
