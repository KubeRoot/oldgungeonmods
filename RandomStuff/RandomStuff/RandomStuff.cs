﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
//using static ETGModConsole;

namespace RandomStuff
{
    class TimeCommandUpdater : MonoBehaviour
    {
        public void Update()
        {
            BraveTime.SetTimeScaleMultiplier(RandomStuff.TimeScale, RandomStuff.gameObject);
        }
    }

    public class RandomStuff : ETGModule
    {
        public static GameObject gameObject = new GameObject("Kube_RandomStuffMod");
        public static float TimeScale = 1f;

        public override void Exit()
        {
        }

        public override void Init()
        {
        }

        public void SetTimeScale(string[] args)
        {
            if (!ETGModConsole.ArgCount(args, 1, 1)) return;
            float timeScale;
            if(!float.TryParse(args[0], out timeScale))
            {
                ETGModConsole.Log("Invalid timescale");
            }
            else
            {
                TimeScale = timeScale;
                ETGModConsole.Log("Timescale set to "+TimeScale.ToString());
            }
        }

        public override void Start()
        {
            BraveTime.RegisterTimeScaleMultiplier(1f, gameObject);
            ETGMod.Time.SetTimeScaleModifierIsPost(true, gameObject);

            UnityEngine.Object.DontDestroyOnLoad(gameObject);
            gameObject.AddComponent<TimeCommandUpdater>();
            ETGModConsole.Commands.AddUnit("timescale", SetTimeScale);

            
        }
    }
}
