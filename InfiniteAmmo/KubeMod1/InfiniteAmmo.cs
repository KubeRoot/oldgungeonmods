﻿using System;
using System.Collections;
using UnityEngine;

class InfAmmoBehaviour : MonoBehaviour
{
    private Gun m_gun;
    private bool m_beam;

    IEnumerator reload(PlayerController ply, Gun gun)
    {
        if (ply.CurrentGun != null)
        {
            yield return null;
            gun.ForceImmediateReload();
        }
    }

    private void OnFinishAttackReload(PlayerController ply, Gun gun)
    {
        if (KubeInfAmmo.isEnabled && gun.DefaultModule.shootStyle != ProjectileModule.ShootStyle.Beam)
            ETGMod.StartGlobalCoroutine(reload(ply, gun));
    }

    private void OnPostFiredReload(PlayerController ply, Gun gun)
    {
        if (KubeInfAmmo.isEnabled && gun.DefaultModule.shootStyle != ProjectileModule.ShootStyle.Beam)
            ETGMod.StartGlobalCoroutine(reload(ply, gun));
    }

    private void OnPostFiredGiveAmmo(PlayerController ply, Gun gun)
    {
        if (KubeInfAmmo.isEnabled)
            gun.ammo = gun.AdjustedMaxAmmo;
    }

    public void Start()
    {
        m_gun = GetComponent<Gun>();
        m_beam = m_gun.DefaultModule.shootStyle == ProjectileModule.ShootStyle.Beam;
        
        m_gun.OnPostFired += OnPostFiredGiveAmmo;

        if (!m_beam)
        {
            m_gun.InfiniteAmmo = true;
            m_gun.OnFinishAttack += OnFinishAttackReload;
            m_gun.OnPostFired += OnPostFiredReload;
        }
    }
}

public class RapidFireBehavior : MonoBehaviour
{
    private bool m_firingtoggle;
    private bool m_primary;
    public PlayerController m_ply;

    private bool clusterFuck
    {
        get
        {
            return KubeInfAmmo.isEnabled && KubeInfAmmo.clusterFuck;
        }
    }
    private int mult
    {
        get
        {
            return KubeInfAmmo.mult;
        }
    }

    private void DoChangeToRandomGun()
    {
        m_ply.ChangeToRandomGun();
        m_ply.CurrentGun.Initialize(m_ply);
        m_ply.Update();
    }

    public void Update()
    {
        if (m_primary&&(Input.GetKey("b")^m_firingtoggle))
        {
            for (int i = 0; i < mult; i++)
            {
                Gun gun = m_ply?.CurrentGun;
                if (gun != null && gun.DefaultModule.shootStyle != ProjectileModule.ShootStyle.Beam)
                {
                    gun.CeaseAttack();
                    gun.ForceImmediateReload();
                    if (clusterFuck)
                        DoChangeToRandomGun();
                    gun.Attack();
                }
                else if (clusterFuck)
                    DoChangeToRandomGun();
            }
        }

        if (Input.GetKeyDown("n"))
        {
            m_firingtoggle = !m_firingtoggle;
        }
    }
    public void Start()
    {
        m_ply = GetComponent<PlayerController>();
        m_primary = m_ply.IsPrimaryPlayer;

        StatModifier rapidfire = new StatModifier();
        rapidfire.statToBoost = PlayerStats.StatType.RateOfFire;
        rapidfire.amount = 9001-m_ply.stats.GetBaseStatValue(PlayerStats.StatType.RateOfFire);
        m_ply.ownerlessStatModifiers.Clear();
        m_ply.ownerlessStatModifiers.Add(rapidfire);
        m_ply.stats.RecalculateStats(m_ply);
    }
}

public class KubeInfAmmo : ETGModule
{
    private static bool enabled;
    public static bool isEnabled
    {
        get
        {
            return enabled;
        }
    }

    public static bool clusterFuck = false;
    public static int mult = 1;

    private void NewGun(Component component)
    {
        Gun gun = component as Gun;

        if (gun.GetComponent<InfAmmoBehaviour>() == null)
        {
            gun.gameObject.AddComponent<InfAmmoBehaviour>();
        }
    }

    private void NewPlayer(Component component)
    {
        PlayerController ply = component as PlayerController;

        ply.gameObject.AddComponent<RapidFireBehavior>();
    }

    private void ClusterFuckCommand(string[] args)
    {
        bool tmp = !clusterFuck;
        if (args.Length > 0) bool.TryParse(args[0], out tmp);

        clusterFuck = tmp;
        ETGModConsole.Log("Ridiculous mode " + (clusterFuck ? "enabled" : "disabled"));
    }

    private void SetMultCommand(string[] args)
    {
        if(args.Length == 0)
        {
            mult = 1;
            ETGModConsole.Log("Reset multiplier");
            return;
        }

        int tmp = 1;
        if (!int.TryParse(args[0], out tmp) || tmp < 1)
        {
            ETGModConsole.Log("Invalid multiplier (Should be Integer >= 1)");
            return;
        }

        mult = tmp;
        ETGModConsole.Log("Set firing multiplier to " + mult);
    }

    public override void Start()
    {
        enabled = true;
    }
    
    public override void Exit()
    {
        enabled = false;
    }

    public override void Init()
    {
        ETGMod.Objects.AddHook<Gun>(NewGun);
        ETGMod.Objects.AddHook<PlayerController>(NewPlayer);

        ETGModConsole.Commands.AddGroup("infammo");

        ETGModConsole.Commands.GetGroup("infammo")
            .AddUnit("ridiculous", ClusterFuckCommand)
            .AddUnit("mult", SetMultCommand);
    }
}